#!/bin/bash
set -e

## copy script to container
#docker container cp function.sql postgres_test:/
docker container cp proc.sql postgres_test:/

## run script inside container
#docker container exec -it postgres_test psql --dbname=suppliers --username postgres -f /proc.sql
docker container exec -it postgres_test psql -d suppliers -U postgres -f /proc.sql
