# docker_postgresql

## create and test postgresql docker container with persisting data (volume mount) on a local host


useful links:
https://www.postgresqltutorial.com/postgresql-python/connect/
https://github.com/jdaarevalo/docker_postgres_with_data
https://docs.docker.com/storage/volumes/
https://github.com/nikskushwah/CRUD_Flask_Docker
https://stackoverflow.com/questions/26598738/how-to-create-user-database-in-script-for-docker-postgres
https://stackoverflow.com/questions/59715622/docker-compose-and-create-db-in-postgres-on-init
